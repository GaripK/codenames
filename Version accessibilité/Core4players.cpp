#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <random>
#include "structure.h"


int fonct_rand()
{
  srand(time(NULL));
  std::mt19937 rng;
  rng.seed(std::random_device()());
  std::uniform_int_distribution<std::mt19937::result_type> dist6(0,rand());

  return (int)dist6(rng);
}

struct jeu *initinput(struct jeu * jeu)
{
  int i,random;
  char equipec='\0';
  char equipe2='\0';
  for(i=0;i<25;i++)
  {
    jeu->input[i][0]='w';
    jeu->input[i][1]='f';
  }
  if(jeu->equipecommance=='r')
  {
    equipec='r';
    equipe2='b';
  }
  else
  {
    equipec='b';
    equipe2='r';
  }
  for(i=0;i<9;i++)
  {
    random=(fonct_rand()%25);
    if(jeu->input[random][0]!=equipec)
    {
      jeu->input[random][0]=equipec;
    }
    else
    {
      i--;
    }
  }
  for(i=0;i<8;i++)
  {
    random=(fonct_rand()%25);
    if((jeu->input[random][0]!=equipec)&&(jeu->input[random][0]!=equipe2))
    {
      jeu->input[random][0]=equipe2;
    }
    else
    {
      i--;
    }
  }
  for(i=0;i<1;i++)
  {
    random=(fonct_rand()%25);
    if((jeu->input[random][0]=='w'))
    {
      jeu->input[random][0]='n';
    }
    else
    {
      i--;
    }
  }
  return jeu;
}
struct jeu *getMots(struct jeu * jeu)
{
  int i=0;
  int j;
  int k;
  int l;
  int boolean=0;
  FILE *fptr;
  char * line = NULL;
  size_t len = 0;
  char filename[100];
  int nbrlines[12];
  nbrlines[0]=2;
  nbrlines[1]=53;
  nbrlines[2]=117;
  nbrlines[3]=179;
  nbrlines[4]=161;
  nbrlines[5]=89;
  nbrlines[6]=45;
  nbrlines[7]=27;
  nbrlines[8]=17;
  nbrlines[9]=6;
  nbrlines[10]=4;


  for(i=0;i<25;i++)
  {
    l=(fonct_rand()%11);
    sprintf(filename, "%s%d%s","MotsA",l+2,"lettres.txt");

    if ((fptr = fopen(filename, "r")) == NULL)
    {
        perror("Error! opening file");
        exit(1);
    }
    for(j=0;j<((fonct_rand()%nbrlines[l])+1);j++)
    {
      len = 0;
      getline(&line, &len, fptr);
    }
    k=0;
    while(line[k]!='\n')
      k++;
    line[k]='\0';
    for(j=0;j<i;j++)
    {
      if(strcmp(jeu->mots[j],line)==0)
      {
        boolean=1;
        break;
      }
      else
      {
        boolean=0;
      }
    }
    if(boolean==0)
    {

      k=0;
      while(line[k]!='\0')
      {
        jeu->mots[i][k]=line[k];
        k++;
      }
    }
    else
    {
      boolean=0;
      i--;
    }
    fclose(fptr);
  }
  return jeu;
}
struct jeu *init()
{
  struct jeu *j=(struct jeu*)malloc(sizeof(struct jeu));;
  int i;
  for(i=0;i<25;i++)
  {
    bzero(j->mots[i],sizeof(j->mots[i]));
    bzero(j->input[i],sizeof(j->input[i]));
  }
  j->nbremottrouve[0]=0;
  j->nbremottrouve[1]=0;
  j->casesjouer=0;
  j->nbremotadev=0;
  bzero(j->output,sizeof(j->output));
  bzero(j->gameover,sizeof(j->gameover));
  if(fonct_rand()%2==0)
  {
    j->equipetour='b';
    j->equipecommance='b';
  }
  else
  {
    j->equipetour='r';
    j->equipecommance='r';
  }
  j=getMots(j);
  j=initinput(j);
  return j;
}

struct jeu * changer_equipe_jouant(struct jeu* j)
{
  if(j->equipetour=='b')
    j->equipetour='r';
  else
    j->equipetour='b';
  return j;
}
struct jeu * verifie_gameover(struct jeu* j)
{
  int i,bleu=0,rouge=0;
  char equipegagne='\0';
  for(i=0;i<25;i++)
  {
    if(j->output[i]=='b')
      bleu++;
    else if(j->output[i]=='r')
      rouge++;
  }
  if(j->equipecommance=='b')
  {
    if((bleu==9)&&(rouge==8))
    {
      equipegagne='2';
      sprintf(j->gameover,"%s'%c'%s","l'equipe ",j->equipetour," à trouver toutes les cartes des deux équipes\n");
      return j;
    }
    else if(bleu==9)
    {
      equipegagne='b';
    }
    else if(rouge==8)
    {
      equipegagne='r';
    }
    if((bleu==9)||(rouge==8))
    {
      sprintf(j->gameover,"%s'%c'%s'%c'%s","l'equipe ",j->equipetour," à trouver toutes les cartes de l'equipe ",equipegagne,"\n");
      return j;
    }
  }
  else if (j->equipecommance=='r')
  {
    if((bleu==8)&&(rouge==9))
    {
      equipegagne='2';
      sprintf(j->gameover,"%s'%c'%s","l'equipe ",j->equipetour," à trouver toutes les cartes des deux équipes\n");
      return j;
    }
    else if(bleu==8)
    {
      equipegagne='b';
    }
    else if(rouge==9)
    {
      equipegagne='r';
    }
    if((bleu==8)||(rouge==9))
    {
      sprintf(j->gameover,"%s'%c'%s'%c'%s","l'equipe ",j->equipetour," à trouver toutes les cartes de l'equipe ",equipegagne,"\n");
      return j;
    }
  }
  return j;
}

struct jeu * jouer(struct jeu* j)
{
    if(j->input[j->casesjouer][1]!='v')
    {
      if(j->input[j->casesjouer][0]=='b')
      {
        j->nbremottrouve[0]++;
      }
      else if(j->input[j->casesjouer][0]=='r')
      {
        j->nbremottrouve[1]++;
      }
      j->output[j->casesjouer]=j->input[j->casesjouer][0];
      j->input[j->casesjouer][1]='v';
      if(j->input[j->casesjouer][0]=='n')
      {
        sprintf(j->gameover,"%s'%c'%s","l'equipe ",j->equipetour," est tomber sur l'assassin\n");
        return j;
      }
      if((j->input[j->casesjouer][0]!=j->equipetour))
      {
        return changer_equipe_jouant(j);
      }
      j->casesjouer=0;
    }
    else
    {
      printf("mot déjà découvert ! au tour de l'autre équipe !\n");
      sleep(5);
      return changer_equipe_jouant(j);
    }
  return j;
}
void affichage_maitre(struct jeu *j2)
{
  int i,l,k;
  printf("\n");
  printf("-----------vu maitre espion-----------\n");
  for(i=0;i<25;i++)
  {
    if(i%5==0)
      printf("\n");
    printf("%d=%c%c:%s",i+1,j2->input[i][0],j2->input[i][1],j2->mots[i]);
    l=0;
    while(j2->mots[i][l]!='\0')
      l++;
    for(k=0;k<(15-l);k++)
      printf(" ");
  }
  printf("\n");
  printf("\n");
}
void affichage_agent(struct jeu *j2)
{
  int i,l,k;
    printf("\n");
    printf("----------------vu agent---------------\n");
    for(i=0;i<25;i++)
    {
      if(i%5==0)
        printf("\n");
      printf("%d=%c:%s",i+1,j2->output[i],j2->mots[i]);
      l=0;
      while(j2->mots[i][l]!='\0')
        l++;
      for(k=0;k<(15-l);k++)
        printf(" ");
    }
    printf("\n");
    printf("\n");

}

int string_to_int(char msg[])
{

  int i=0,num=0,q=0;
  if(!msg[i] || msg[i]<'0' || msg[i]>'9' )
  {
    printf("invalid number\n");
    sleep(5);
    return(-1);
  }

  q=i+1;
  num=msg[i]-'0';
  while(msg[q]>='0' && msg[q]<='9')
  {
    num=10*num+(msg[q]-'0');
    q++;
  }
  if(msg[q]!='\n')
  {
    printf("invalid number\n");
    sleep(5);
    return(-1);
  }
  return num;
}

int jouer_un_mot(struct jeu *j2, int tour, char inittour)
{
  int m,booltour2=0,nbremotmax=0,nbremottrouve=0,num=0;
  char msg[200];
  for(m=0;m<j2->nbremotadev;m++)
  {
    system("clear");
    printf("\n");
    printf("--------------------------------------\n");
    printf("---------------tour n°%d:%c-------------\n",tour,j2->equipetour);
    printf("--------------------------------------\n");
    if(booltour2==0)
    {
      affichage_agent(j2);
    }
    if(booltour2==1)
    {
      affichage_maitre(j2);
    }
    booltour2=0;

    printf("tapez la position des mots à deviner ('exit' pour quiter)\n");
    printf("il reste %d mots à deviner\n",j2->nbremotadev-m);
    printf("(de haut en bas de gauche a droite de 1 à 25)\n");
    printf("('maitre' pour voir la version maitre-espion)\n");
    printf("('agent' pour voir la version agent)\n");
    printf("\n");

    if(j2->equipecommance==j2->equipetour)
      nbremotmax=9;
    else
      nbremotmax=8;

    if(j2->equipetour=='b')
      nbremottrouve=j2->nbremottrouve[0];
    else if(j2->equipetour=='r')
      nbremottrouve=j2->nbremottrouve[1];

    printf("vous avez deviné %d mots, il vous en reste %d à trouver\n",nbremottrouve,nbremotmax-nbremottrouve);
    printf("\n");

    bzero(msg,sizeof(msg));

    sprintf(msg,"tour des agents mot n°%d ('passe' pour passer) > ",m+1);
    write(STDIN_FILENO,msg,sizeof(msg));

    bzero(msg,sizeof(msg));

    read(STDIN_FILENO,msg,sizeof(msg));

    if(strncmp(msg,"exit",4)==0)
    {
      printf("-----------------exit-----------------\n");
      return(0);
    }

    if(strncmp(msg,"maitre",6)==0)
    {
      m--;
      booltour2=1;
      continue;
    }

    if(strncmp(msg,"agent",5)==0)
    {
      system("clear");
      affichage_agent(j2);
      m--;
      continue;
    }
    if(strncmp(msg,"passe",5)==0)
    {
      j2=changer_equipe_jouant(j2);
      break;
    }
    num=0;
    num=string_to_int(msg);

    if(num==-1)
    {
      m--;
      continue;
    }
    if((num==0)||(num>25))
    {
      printf("le nombre %d doit être entre 1 et 25\n",num);
      sleep(5);
      m--;
      continue;
    }

    j2->casesjouer=num-1;

    j2=jouer(j2);


    if(j2->gameover[0]!='\0')
    {
      printf("%s",j2->gameover);
      return(0);
    }

    j2=verifie_gameover(j2);
    if(j2->gameover[0]!='\0')
    {
      printf("%s",j2->gameover);
      return(0);
    }
    if(j2->equipetour!=inittour)
    {
      break;
    }
  }
  return(1);
}

int jouer_une_partie()
{
  struct jeu *j2;
  char msg[200];
  char inittour='\0';
  j2=init();
  int num,tour=0;
  int booltour1=0;
  int nbremottrouve=0,nbremotmax=0;
  while(1)
  {
    j2->nbremotadev=0;
    tour++;

    system("clear");
    printf("\n");
    printf("--------------------------------------\n");
    printf("---------------tour n°%d:%c-------------\n",tour,j2->equipetour);
    printf("--------------------------------------\n");

    if(booltour1==0)
    {
      affichage_agent(j2);
    }
    if(booltour1==1)
    {
      affichage_maitre(j2);
    }
    booltour1=0;

    printf("c'est à léquipe '%c' de jouer ('exit' pour quiter) !\n",j2->equipetour);
    printf("('maitre' pour voir la version maitre-espion)\n");
    printf("('agent' pour voir la version agent)\n");
    printf("\n");

    if(j2->equipecommance==j2->equipetour)
      nbremotmax=9;
    else
      nbremotmax=8;

    if(j2->equipetour=='b')
      nbremottrouve=j2->nbremottrouve[0];
    else if(j2->equipetour=='r')
      nbremottrouve=j2->nbremottrouve[1];

    printf("vous avez deviné %d mots, il vous en reste %d à trouver\n",nbremottrouve,nbremotmax-nbremottrouve);
    printf("\n");

    write(STDIN_FILENO,"tour du maitre espion tapez le nombre de mots à deviner > ",sizeof("tour du maitre espion tapez le nombre de mots à deviner > "));

    bzero(msg,sizeof(msg));

    read(STDIN_FILENO,msg,sizeof(msg));

    if(strncmp(msg,"exit",4)==0)
    {
      printf("-----------------exit-----------------\n");
      free(j2);
      return(0);
    }
    if(strncmp(msg,"agent",5)==0)
    {
      system("clear");
      affichage_agent(j2);
      tour--;
      continue;
    }

    if(strncmp(msg,"maitre",6)==0)
    {
      booltour1=1;
      tour--;
      continue;
    }


    num=string_to_int(msg);

    if(num==-1)
    {
      tour--;
      continue;
    }
    if( (num==0) || (num>(nbremotmax-nbremottrouve)) )
    {
      printf("le nombre doit être compris entre 1 et %d\n",nbremotmax-nbremottrouve);
      sleep(5);
      tour--;
      continue;
    }

    j2->nbremotadev=num;


    inittour=j2->equipetour;
    if(jouer_un_mot(j2, tour, inittour)==0)
    {
      sleep(5);
      break;
    }

  }
  free(j2);
  return(0);
}
int main()
{
  char msg[200];
  while(1)
  {
    system("clear");
    printf("\n");
    printf("--------------------------------------\n");
    printf("--------------CODE NAME---------------\n");
    printf("--------------------------------------\n");
    printf("\n");
    write(STDIN_FILENO,"'exit' pour exit ou 'start' pour commencer> ",sizeof("'exit' pour exit ou 'start' pour commencer> "));

    bzero(msg,sizeof(msg));

    read(STDIN_FILENO,msg,sizeof(msg));

    if(strncmp(msg,"exit",4)==0)
    {
      printf("-----------------exit-----------------\n");
      return(0);
    }
    else if(strncmp(msg,"start",5)==0)
    {
      jouer_une_partie();
      continue;
    }
    else
    {
      printf("invalide commande\n");
      sleep(5);
      continue;
    }
  }
}
