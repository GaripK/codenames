#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
    // Here is a small helper for you! Have a look.
#include "ResourcePath.hpp"


#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <random>
#include <codecvt>
#include "structure.h"


std::wstring stringToWstring(const std::string& t_str)
{
        //setup converter
    typedef std::codecvt_utf8<wchar_t> convert_type;
    std::wstring_convert<convert_type, wchar_t> converter;

        //use converter (.to_bytes: wstr->str, .from_bytes: str->wstr)
    return converter.from_bytes(t_str);
}

int fonct_rand()
{
    srand(time(NULL));
    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(0,rand());

    return (int)dist6(rng);
}

struct jeu *initinput(struct jeu * jeu)
{
    int i,random;
    char equipec='\0';
    char equipe2='\0';
    for(i=0;i<25;i++)
    {
        jeu->input[i][0]='w';
        jeu->input[i][1]='f';
    }
    if(jeu->equipecommance=='r')
    {
        equipec='r';
        equipe2='b';
    }
    else
    {
        equipec='b';
        equipe2='r';
    }
    for(i=0;i<9;i++)
    {
        random=(fonct_rand()%25);
        if(jeu->input[random][0]!=equipec)
        {
            jeu->input[random][0]=equipec;
        }
        else
        {
            i--;
        }
    }
    for(i=0;i<8;i++)
    {
        random=(fonct_rand()%25);
        if((jeu->input[random][0]!=equipec)&&(jeu->input[random][0]!=equipe2))
        {
            jeu->input[random][0]=equipe2;
        }
        else
        {
            i--;
        }
    }
    for(i=0;i<1;i++)
    {
        random=(fonct_rand()%25);
        if(jeu->input[random][0]=='w')
        {
            jeu->input[random][0]='n';
        }
        else
        {
            i--;
        }
    }
    return jeu;
}

struct jeu *getMots(struct jeu * jeu)
{
    int i=0;
    int j;
    int k;
    int l;
    int boolean=0;
    FILE *fptr;
    char * line = NULL;
    size_t len = 0;
    char filename[1000];
    int nbrlines[12];
    nbrlines[0]=2;
    nbrlines[1]=53;
    nbrlines[2]=117;
    nbrlines[3]=179;
    nbrlines[4]=161;
    nbrlines[5]=89;
    nbrlines[6]=45;
    nbrlines[7]=27;
    nbrlines[8]=17;
    nbrlines[9]=6;
    nbrlines[10]=4;


    for(i=0;i<25;i++)
    {
        l=(fonct_rand()%8);
        sprintf(filename, "%s%s%d%s",resourcePath().c_str(),"MotsA",l+2,"lettres.txt");

        if ((fptr = fopen(filename, "r")) == NULL)
        {
            perror("Error! opening file");
            exit(1);
        }
        for(j=0;j<((fonct_rand()%nbrlines[l])+1);j++)
        {
            len = 0;
            getline(&line, &len, fptr);
        }
        k=0;
        while(line[k]!='\n')
            k++;
        line[k]='\0';
        for(j=0;j<i;j++)
        {
            if(strcmp(jeu->mots[j],line)==0)
            {
                boolean=1;
                break;
            }
            else
            {
                boolean=0;
            }
        }
        if(boolean==0)
        {

            k=0;
            while(line[k]!='\0')
            {
                jeu->mots[i][k]=line[k];
                k++;
            }
        }
        else
        {
            boolean=0;
            i--;
        }
        fclose(fptr);
    }
    return jeu;
}

struct jeu *init()
{
    struct jeu *j=(struct jeu*)malloc(sizeof(struct jeu));;
    int i;
    for(i=0;i<25;i++)
    {
        bzero(j->mots[i],sizeof(j->mots[i]));
        bzero(j->input[i],sizeof(j->input[i]));
    }
    j->nbremottrouve[0]=0;
    j->nbremottrouve[1]=0;
    j->casesjouer=0;
    j->nbremotadev=0;
    j->equipegagne=NULL;
    bzero(j->output,sizeof(j->output));
    if(fonct_rand()%2==0)
    {
        j->equipetour='b';
        j->equipecommance='b';
    }
    else
    {
        j->equipetour='r';
        j->equipecommance='r';
    }
    j=getMots(j);
    j=initinput(j);
    return j;
}

struct jeu * changer_equipe_jouant(struct jeu* j)
{
    if(j->equipetour=='b')
        j->equipetour='r';
    else
        j->equipetour='b';
    return j;
}

struct jeu * verifie_gameover(struct jeu* j)
{
    int i,bleu=0,rouge=0;
    for(i=0;i<25;i++)
    {
        if(j->output[i]=='b') {
            bleu++;
        }
        else if(j->output[i]=='r') {
            rouge++;
        }

    }
    if(j->equipecommance=='b')
    {
        if((bleu==9)&&(rouge==8))
        {
            j -> equipegagne='2';
            return j;
        }
        else if(bleu==9)
        {
            j -> equipegagne='b';
        }
        else if(rouge==8)
        {
            j -> equipegagne='r';
        }
        if((bleu==9)||(rouge==8))
        {
            return j;
        }
    }
    else if (j->equipecommance=='r')
    {
        if((bleu==8)&&(rouge==9))
        {
            j -> equipegagne='2';
        }
        else if(bleu==8)
        {
            j -> equipegagne='b';
        }
        else if(rouge==9)
        {
            j -> equipegagne='r';
        }
        if((bleu==8)||(rouge==9))
        {
            return j;
        }
    }
    return j;
}

struct jeu * jouer(struct jeu* j)
{
    if(j->input[j->casesjouer][1]!='v')
    {
        if(j->input[j->casesjouer][0]=='b')
        {
            j->nbremottrouve[0]++;
        }
        else if(j->input[j->casesjouer][0]=='r')
        {
            j->nbremottrouve[1]++;
        }
        j->output[j->casesjouer]=j->input[j->casesjouer][0];
        j->input[j->casesjouer][1]='v';
        if(j->input[j->casesjouer][0]=='n')
        {
            printf("%s'%c'%s","l'equipe ",j->equipetour," est tomber sur l'assassin\n");
            if (j->equipetour == 'b') {
                j->equipegagne = 'r';
            } else {
                j->equipegagne = 'b';
            }
            return j;
        }
        if((j->input[j->casesjouer][0]!=j->equipetour))
        {
            return changer_equipe_jouant(j);
        }
        j->casesjouer=0;
    }
    else
    {
        printf("mot déjà découvert ! au tour de l'autre équipe !\n");
        return changer_equipe_jouant(j);
    }
    return j;
}


int main()
{
        //////////////////////////////////////////////// BACKGROUND + FENETRE + SCALING
    sf::RenderWindow menu(sf::VideoMode(960, 540), "Code Names");
    sf::Texture background;
    if (!background.loadFromFile(resourcePath() + "Menuvide.png"))
        return EXIT_FAILURE;
    sf::Sprite back;
    sf::Vector2u TextureSize;
    sf::Vector2u WindowSize;
    TextureSize = background.getSize();
    WindowSize = menu.getSize();
    float ScaleX = (float)WindowSize.x / TextureSize.x;
    float ScaleY = (float)WindowSize.y / TextureSize.y;
    back.setTexture(background);
    back.setScale(ScaleX, ScaleY);

    struct jeu *j = init();


        // CHARGEMENT DE LA POLICE


    sf::Font font;
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        return EXIT_FAILURE;
    }

        // CREATION DES TEXTES

    sf::Text text_1;
    text_1.setFont(font);
    text_1.setString("Jouer");
    text_1.setCharacterSize(50);
    text_1.setFillColor(sf::Color::Black);
    text_1.setPosition(sf::Vector2f(440, 210));

    sf::Text text_2;
    text_2.setFont(font);
    text_2.setString("Tutoriel");
    text_2.setCharacterSize(50);
    text_2.setFillColor(sf::Color::Black);
    text_2.setPosition(sf::Vector2f(440, 290));

    sf::Text text_3;
    text_3.setFont(font);
    text_3.setString("Options");
    text_3.setCharacterSize(50);
    text_3.setFillColor(sf::Color::Black);
    text_3.setPosition(sf::Vector2f(420, 370));

    sf::Text text_4;
    text_4.setFont(font);
    text_4.setString("Quitter");
    text_4.setCharacterSize(50);
    text_4.setFillColor(sf::Color::Black);
    text_4.setPosition(sf::Vector2f(440, 450));

        // CHARGEMENT TEXTURE BOUTTONS

    sf::Texture boutton;
    if (!boutton.loadFromFile(resourcePath() + "Boutonmenu.png"))
    {
        return EXIT_FAILURE;
    }

        // CREATION DES BOUTTONS

    sf::RectangleShape boutton1;                            // <- CrÈation de l'instance
    boutton1.setSize(sf::Vector2f(500, 70));                        // la taille
    boutton1.setPosition(sf::Vector2f(230, 210));                // <- gËrer le scaling
    boutton1.setTexture(&boutton);                            // <- on leur donne la texture

    sf::RectangleShape boutton2;
    boutton2.setSize(sf::Vector2f(500, 70));
    boutton2.setPosition(sf::Vector2f(230, 290));
    boutton2.setTexture(&boutton);

    sf::RectangleShape boutton3;
    boutton3.setSize(sf::Vector2f(500, 70));
    boutton3.setPosition(sf::Vector2f(230, 370));
    boutton3.setTexture(&boutton);

    sf::RectangleShape boutton4;
    boutton4.setSize(sf::Vector2f(500, 70));
    boutton4.setPosition(sf::Vector2f(230, 450));
    boutton4.setTexture(&boutton);

        // OUVERTURE DE LA FENETRE

    while (menu.isOpen())
    {
        sf::Event event;
        while (menu.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                menu.close();
        }

            // LES JOLIS DESSINS

        menu.clear();
        menu.draw(back);
        menu.draw(boutton1);
        menu.draw(boutton2);
        menu.draw(boutton3);
        menu.draw(boutton4);
        menu.draw(text_1);
        menu.draw(text_2);
        menu.draw(text_3);
        menu.draw(text_4);
        menu.display();

            // ON ENREGISTRE  UN EVENT

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
                // RECUPERE LA POSITION DE LA SOURIS AU MOMENT DU CLIC
            sf::Vector2f mouse = menu.mapPixelToCoords(sf::Mouse::getPosition(menu));

                // RECUPERE LA POSITION DES BOUTTONS
            sf::FloatRect locationboutton_1 = boutton1.getGlobalBounds();
            sf::FloatRect locationboutton_2 = boutton2.getGlobalBounds();
            sf::FloatRect locationboutton_3 = boutton3.getGlobalBounds();
            sf::FloatRect locationboutton_4 = boutton4.getGlobalBounds();

                // TRAITEMENT DE QUEL BOUTON EST CLIQUE
                //TODO

            if (locationboutton_4.contains(mouse))
            {
                menu.close();
            }

            if (locationboutton_2.contains(mouse))  // Tuto
            {
                sf::RenderWindow tuto(sf::VideoMode(960, 540), "Tutoriel");
                sf::Texture guide;
                if (!guide.loadFromFile(resourcePath() + "Tuto.png"))
                    return EXIT_FAILURE;
                sf::Sprite image;
                sf::Vector2u TextureSize;
                sf::Vector2u WindowSize;
                TextureSize = guide.getSize();
                WindowSize = tuto.getSize();
                float ScaleX = (float)WindowSize.x / TextureSize.x;
                float ScaleY = (float)WindowSize.y / TextureSize.y;
                image.setTexture(guide);
                image.setScale(ScaleX, ScaleY);

                while (tuto.isOpen())
                {
                    sf::Event event;
                    while (tuto.pollEvent(event))
                    {
                        if (event.type == sf::Event::Closed)
                            tuto.close();
                    }

                        // LES JOLIS DESSINS

                    tuto.clear();
                    tuto.draw(image);
                    tuto.display();
                }

            }

            if (locationboutton_1.contains(mouse))  // C'est le bouton si on lance le jeu
            {
                menu.close();
                sf::RenderWindow game(sf::VideoMode(960, 540), "4 joueurs");
                game.setVerticalSyncEnabled(true);

                sf::Texture fondb;
                sf::Texture fondr;
                if (!fondb.loadFromFile(resourcePath() + "Plateauvidebleue.png"))
                    return EXIT_FAILURE;
                if (!fondr.loadFromFile(resourcePath() + "Plateauviderouge.png"))
                    return EXIT_FAILURE;


                    // On crée un fond bleu
                sf::Sprite fondbleu;
                sf::Vector2u TextureSize_1;
                sf::Vector2u WindowSize_1;
                TextureSize_1 = fondb.getSize();
                WindowSize_1 = game.getSize();
                float ScaleX_1 = (float)WindowSize_1.x / TextureSize_1.x;
                float ScaleY_1 = (float)WindowSize_1.y / TextureSize_1.y;
                fondbleu.setTexture(fondb);
                fondbleu.setScale(ScaleX_1, ScaleY_1);

                    // Et un rouge
                sf::Sprite fondrouge;
                sf::Vector2u TextureSize_2;
                sf::Vector2u WindowSize_2;
                TextureSize_2 = fondr.getSize();
                WindowSize_2 = game.getSize();
                float ScaleX_2 = (float)WindowSize_2.x / TextureSize_2.x;
                float ScaleY_2 = (float)WindowSize_2.y / TextureSize_2.y;
                fondrouge.setTexture(fondr);
                fondrouge.setScale(ScaleX_2, ScaleY_2);


                sf::Texture postitblanc;
                if (!postitblanc.loadFromFile(resourcePath() + "Postitblanc.png"))
                {
                    return EXIT_FAILURE;
                }

                sf::Texture postitbleu;
                if (!postitbleu.loadFromFile(resourcePath() + "Postitbleu.png"))
                {
                    return EXIT_FAILURE;
                }

                sf::Texture postitrouge;
                if (!postitrouge.loadFromFile(resourcePath() + "Postitrouge.png"))
                {
                    return EXIT_FAILURE;
                }

                sf::Texture postitnoir;
                if (!postitnoir.loadFromFile(resourcePath() + "Postitnoir.png"))
                {
                    return EXIT_FAILURE;
                }

                sf::Texture postitneutre;
                if (!postitneutre.loadFromFile(resourcePath() + "Postitneutre.png"))
                {
                    return EXIT_FAILURE;
                }

                sf::Font font;
                if (!font.loadFromFile(resourcePath() + "Caveat.ttf"))
                {
                    return EXIT_FAILURE;
                }


                    // On crée les 25 post-it

                    // 1
                sf::RectangleShape postit1;
                postit1.setSize(sf::Vector2f(100, 90));
                postit1.setPosition(sf::Vector2f(50, 40));
                postit1.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext1;
                postittext1.setFont(font);
                postittext1.setString(stringToWstring(j -> mots[1-1]));
                postittext1.setCharacterSize(22);
                postittext1.setFillColor(sf::Color::Black);
                postittext1.setPosition(sf::Vector2f(60, 65));

                    // 2
                sf::RectangleShape postit2;
                postit2.setSize(sf::Vector2f(100, 90));
                postit2.setPosition(sf::Vector2f(160, 40));
                postit2.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext2;
                postittext2.setFont(font);
                postittext2.setString(stringToWstring(j -> mots[2-1]));
                postittext2.setCharacterSize(22);
                postittext2.setFillColor(sf::Color::Black);
                postittext2.setPosition(sf::Vector2f(170, 65));

                    // 3
                sf::RectangleShape postit3;
                postit3.setSize(sf::Vector2f(100, 90));
                postit3.setPosition(sf::Vector2f(270, 40));
                postit3.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext3;
                postittext3.setFont(font);
                postittext3.setString(stringToWstring(j -> mots[3-1]));
                postittext3.setCharacterSize(22);
                postittext3.setFillColor(sf::Color::Black);
                postittext3.setPosition(sf::Vector2f(280, 65));

                    // 4
                sf::RectangleShape postit4;
                postit4.setSize(sf::Vector2f(100, 90));
                postit4.setPosition(sf::Vector2f(380, 40));
                postit4.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext4;
                postittext4.setFont(font);
                postittext4.setString(stringToWstring(j -> mots[4-1]));
                postittext4.setCharacterSize(22);
                postittext4.setFillColor(sf::Color::Black);
                postittext4.setPosition(sf::Vector2f(390, 65));

                    // 5
                sf::RectangleShape postit5;
                postit5.setSize(sf::Vector2f(100, 90));
                postit5.setPosition(sf::Vector2f(490, 40));
                postit5.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext5;
                postittext5.setFont(font);
                postittext5.setString(stringToWstring(j -> mots[5-1]));
                postittext5.setCharacterSize(22);
                postittext5.setFillColor(sf::Color::Black);
                postittext5.setPosition(sf::Vector2f(500, 65));

                    // 6
                sf::RectangleShape postit6;
                postit6.setSize(sf::Vector2f(100, 90));
                postit6.setPosition(sf::Vector2f(50, 135));
                postit6.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext6;
                postittext6.setFont(font);
                postittext6.setString(stringToWstring(j -> mots[6-1]));
                postittext6.setCharacterSize(22);
                postittext6.setFillColor(sf::Color::Black);
                postittext6.setPosition(sf::Vector2f(60, 160));

                    // 7
                sf::RectangleShape postit7;
                postit7.setSize(sf::Vector2f(100, 90));
                postit7.setPosition(sf::Vector2f(160, 135));
                postit7.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext7;
                postittext7.setFont(font);
                postittext7.setString(stringToWstring(j -> mots[7-1]));
                postittext7.setCharacterSize(22);
                postittext7.setFillColor(sf::Color::Black);
                postittext7.setPosition(sf::Vector2f(170, 160));

                    // 8
                sf::RectangleShape postit8;
                postit8.setSize(sf::Vector2f(100, 90));
                postit8.setPosition(sf::Vector2f(270, 135));
                postit8.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext8;
                postittext8.setFont(font);
                postittext8.setString(stringToWstring(j -> mots[8-1]));
                postittext8.setCharacterSize(22);
                postittext8.setFillColor(sf::Color::Black);
                postittext8.setPosition(sf::Vector2f(280, 160));

                    // 9
                sf::RectangleShape postit9;
                postit9.setSize(sf::Vector2f(100, 90));
                postit9.setPosition(sf::Vector2f(380, 135));
                postit9.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext9;
                postittext9.setFont(font);
                postittext9.setString(stringToWstring(j -> mots[9-1]));
                postittext9.setCharacterSize(22);
                postittext9.setFillColor(sf::Color::Black);
                postittext9.setPosition(sf::Vector2f(390, 160));

                    // 10
                sf::RectangleShape postit10;
                postit10.setSize(sf::Vector2f(100, 90));
                postit10.setPosition(sf::Vector2f(490, 135));
                postit10.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext10;
                postittext10.setFont(font);
                postittext10.setString(stringToWstring(j -> mots[10-1]));
                postittext10.setCharacterSize(22);
                postittext10.setFillColor(sf::Color::Black);
                postittext10.setPosition(sf::Vector2f(500, 160));

                    // 11
                sf::RectangleShape postit11;
                postit11.setSize(sf::Vector2f(100, 90));
                postit11.setPosition(sf::Vector2f(50, 230));
                postit11.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext11;
                postittext11.setFont(font);
                postittext11.setString(stringToWstring(j -> mots[11-1]));
                postittext11.setCharacterSize(22);
                postittext11.setFillColor(sf::Color::Black);
                postittext11.setPosition(sf::Vector2f(60, 255));

                    // 12
                sf::RectangleShape postit12;
                postit12.setSize(sf::Vector2f(100, 90));
                postit12.setPosition(sf::Vector2f(160, 230));
                postit12.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext12;
                postittext12.setFont(font);
                postittext12.setString(stringToWstring(j -> mots[12-1]));
                postittext12.setCharacterSize(22);
                postittext12.setFillColor(sf::Color::Black);
                postittext12.setPosition(sf::Vector2f(170, 255));

                    // 13
                sf::RectangleShape postit13;
                postit13.setSize(sf::Vector2f(100, 90));
                postit13.setPosition(sf::Vector2f(270, 230));
                postit13.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext13;
                postittext13.setFont(font);
                postittext13.setString(stringToWstring(j -> mots[13-1]));
                postittext13.setCharacterSize(22);
                postittext13.setFillColor(sf::Color::Black);
                postittext13.setPosition(sf::Vector2f(280, 255));

                    // 14
                sf::RectangleShape postit14;
                postit14.setSize(sf::Vector2f(100, 90));
                postit14.setPosition(sf::Vector2f(380, 230));
                postit14.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext14;
                postittext14.setFont(font);
                postittext14.setString(stringToWstring(j -> mots[14-1]));
                postittext14.setCharacterSize(22);
                postittext14.setFillColor(sf::Color::Black);
                postittext14.setPosition(sf::Vector2f(390, 255));

                    // 15
                sf::RectangleShape postit15;
                postit15.setSize(sf::Vector2f(100, 90));
                postit15.setPosition(sf::Vector2f(490, 230));
                postit15.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext15;
                postittext15.setFont(font);
                postittext15.setString(stringToWstring(j -> mots[15-1]));
                postittext15.setCharacterSize(22);
                postittext15.setFillColor(sf::Color::Black);
                postittext15.setPosition(sf::Vector2f(500, 255));

                    // 16
                sf::RectangleShape postit16;
                postit16.setSize(sf::Vector2f(100, 90));
                postit16.setPosition(sf::Vector2f(50, 325));
                postit16.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext16;
                postittext16.setFont(font);
                postittext16.setString(stringToWstring(j -> mots[16-1]));
                postittext16.setCharacterSize(22);
                postittext16.setFillColor(sf::Color::Black);
                postittext16.setPosition(sf::Vector2f(60, 350));

                    // 17
                sf::RectangleShape postit17;
                postit17.setSize(sf::Vector2f(100, 90));
                postit17.setPosition(sf::Vector2f(160, 325));
                postit17.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext17;
                postittext17.setFont(font);
                postittext17.setString(stringToWstring(j -> mots[17-1]));
                postittext17.setCharacterSize(22);
                postittext17.setFillColor(sf::Color::Black);
                postittext17.setPosition(sf::Vector2f(170, 350));

                    // 18
                sf::RectangleShape postit18;
                postit18.setSize(sf::Vector2f(100, 90));
                postit18.setPosition(sf::Vector2f(270, 325));
                postit18.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext18;
                postittext18.setFont(font);
                postittext18.setString(stringToWstring(j -> mots[18-1]));
                postittext18.setCharacterSize(22);
                postittext18.setFillColor(sf::Color::Black);
                postittext18.setPosition(sf::Vector2f(280, 350));

                    // 19
                sf::RectangleShape postit19;
                postit19.setSize(sf::Vector2f(100, 90));
                postit19.setPosition(sf::Vector2f(380, 325));
                postit19.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext19;
                postittext19.setFont(font);
                postittext19.setString(stringToWstring(j -> mots[19-1]));
                postittext19.setCharacterSize(22);
                postittext19.setFillColor(sf::Color::Black);
                postittext19.setPosition(sf::Vector2f(390, 350));

                    // 20
                sf::RectangleShape postit20;
                postit20.setSize(sf::Vector2f(100, 90));
                postit20.setPosition(sf::Vector2f(490, 325));
                postit20.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext20;
                postittext20.setFont(font);
                postittext20.setString(stringToWstring(j -> mots[20-1]));
                postittext20.setCharacterSize(22);
                postittext20.setFillColor(sf::Color::Black);
                postittext20.setPosition(sf::Vector2f(500, 350));

                    // 21
                sf::RectangleShape postit21;
                postit21.setSize(sf::Vector2f(100, 90));
                postit21.setPosition(sf::Vector2f(50, 420));
                postit21.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext21;
                postittext21.setFont(font);
                postittext21.setString(stringToWstring(j -> mots[21-1]));
                postittext21.setCharacterSize(22);
                postittext21.setFillColor(sf::Color::Black);
                postittext21.setPosition(sf::Vector2f(60, 445));

                    // 22
                sf::RectangleShape postit22;
                postit22.setSize(sf::Vector2f(100, 90));
                postit22.setPosition(sf::Vector2f(160, 420));
                postit22.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext22;
                postittext22.setFont(font);
                postittext22.setString(stringToWstring(j -> mots[22-1]));
                postittext22.setCharacterSize(22);
                postittext22.setFillColor(sf::Color::Black);
                postittext22.setPosition(sf::Vector2f(170, 445));

                    // 23
                sf::RectangleShape postit23;
                postit23.setSize(sf::Vector2f(100, 90));
                postit23.setPosition(sf::Vector2f(270, 420));
                postit23.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext23;
                postittext23.setFont(font);
                postittext23.setString(stringToWstring(j -> mots[23-1]));
                postittext23.setCharacterSize(22);
                postittext23.setFillColor(sf::Color::Black);
                postittext23.setPosition(sf::Vector2f(280, 445));

                    // 24
                sf::RectangleShape postit24;
                postit24.setSize(sf::Vector2f(100, 90));
                postit24.setPosition(sf::Vector2f(380, 420));
                postit24.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext24;
                postittext24.setFont(font);
                postittext24.setString(stringToWstring(j -> mots[24-1]));
                postittext24.setCharacterSize(22);
                postittext24.setFillColor(sf::Color::Black);
                postittext24.setPosition(sf::Vector2f(390, 445));

                    // 25
                sf::RectangleShape postit25;
                postit25.setSize(sf::Vector2f(100, 90));
                postit25.setPosition(sf::Vector2f(490, 420));
                postit25.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                sf::Text postittext25;
                postittext25.setFont(font);
                postittext25.setString(stringToWstring(j -> mots[25-1]));
                postittext25.setCharacterSize(22);
                postittext25.setFillColor(sf::Color::Black);
                postittext25.setPosition(sf::Vector2f(500, 445));


                    // On crée l'interrupteur
                sf::Texture interrupteuroff;
                if (!interrupteuroff.loadFromFile(resourcePath() + "Interrupteuroff.png"))
                {
                    return EXIT_FAILURE;
                }

                sf::RectangleShape interrupteur1;
                interrupteur1.setSize(sf::Vector2f(110, 166));
                interrupteur1.setPosition(sf::Vector2f(670, 340));
                interrupteur1.setTexture(&interrupteuroff);


                while (game.isOpen())
                {

                        // On gère la couleur des post-it

                        // 1
                    if (j -> input[1-1][1] == 'v') {
                        if (j -> input[1-1][0] == 'w') {
                            postit1.setTexture(&postitneutre);
                        }
                        if (j -> input[1-1][0] == 'n') {
                            postit1.setTexture(&postitnoir);
                        }
                        if (j -> input[1-1][0] == 'b') {
                            postit1.setTexture(&postitbleu);
                        }
                        if (j -> input[1-1][0] == 'r') {
                            postit1.setTexture(&postitrouge);
                        }
                    } else {
                        postit1.setTexture(&postitblanc);
                    }

                        // 2
                    if (j -> input[2-1][1] == 'v') {
                        if (j -> input[2-1][0] == 'w') {
                            postit2.setTexture(&postitneutre);
                        }
                        if (j -> input[2-1][0] == 'n') {
                            postit2.setTexture(&postitnoir);
                        }
                        if (j -> input[2-1][0] == 'b') {
                            postit2.setTexture(&postitbleu);
                        }
                        if (j -> input[2-1][0] == 'r') {
                            postit2.setTexture(&postitrouge);
                        }
                    } else {
                        postit2.setTexture(&postitblanc);
                    }

                        // 3
                    if (j -> input[3-1][1] == 'v') {
                        if (j -> input[3-1][0] == 'w') {
                            postit3.setTexture(&postitneutre);
                        }
                        if (j -> input[3-1][0] == 'n') {
                            postit3.setTexture(&postitnoir);
                        }
                        if (j -> input[3-1][0] == 'b') {
                            postit3.setTexture(&postitbleu);
                        }
                        if (j -> input[3-1][0] == 'r') {
                            postit3.setTexture(&postitrouge);
                        }
                    } else {
                        postit3.setTexture(&postitblanc);
                    }

                        // 4
                    if (j -> input[4-1][1] == 'v') {
                        if (j -> input[4-1][0] == 'w') {
                            postit4.setTexture(&postitneutre);
                        }
                        if (j -> input[4-1][0] == 'n') {
                            postit4.setTexture(&postitnoir);
                        }
                        if (j -> input[4-1][0] == 'b') {
                            postit4.setTexture(&postitbleu);
                        }
                        if (j -> input[4-1][0] == 'r') {
                            postit4.setTexture(&postitrouge);
                        }
                    } else {
                        postit4.setTexture(&postitblanc);
                    }

                        // 5
                    if (j -> input[5-1][1] == 'v') {
                        if (j -> input[5-1][0] == 'w') {
                            postit5.setTexture(&postitneutre);
                        }
                        if (j -> input[5-1][0] == 'n') {
                            postit5.setTexture(&postitnoir);
                        }
                        if (j -> input[5-1][0] == 'b') {
                            postit5.setTexture(&postitbleu);
                        }
                        if (j -> input[5-1][0] == 'r') {
                            postit5.setTexture(&postitrouge);
                        }
                    } else {
                        postit5.setTexture(&postitblanc);
                    }

                        // 6
                    if (j -> input[6-1][1] == 'v') {
                        if (j -> input[6-1][0] == 'w') {
                            postit6.setTexture(&postitneutre);
                        }
                        if (j -> input[6-1][0] == 'n') {
                            postit6.setTexture(&postitnoir);
                        }
                        if (j -> input[6-1][0] == 'b') {
                            postit6.setTexture(&postitbleu);
                        }
                        if (j -> input[6-1][0] == 'r') {
                            postit6.setTexture(&postitrouge);
                        }
                    } else {
                        postit6.setTexture(&postitblanc);
                    }

                        // 7
                    if (j -> input[7-1][1] == 'v') {
                        if (j -> input[7-1][0] == 'w') {
                            postit7.setTexture(&postitneutre);
                        }
                        if (j -> input[7-1][0] == 'n') {
                            postit7.setTexture(&postitnoir);
                        }
                        if (j -> input[7-1][0] == 'b') {
                            postit7.setTexture(&postitbleu);
                        }
                        if (j -> input[7-1][0] == 'r') {
                            postit7.setTexture(&postitrouge);
                        }
                    } else {
                        postit7.setTexture(&postitblanc);
                    }

                        // 8
                    if (j -> input[8-1][1] == 'v') {
                        if (j -> input[8-1][0] == 'w') {
                            postit8.setTexture(&postitneutre);
                        }
                        if (j -> input[8-1][0] == 'n') {
                            postit8.setTexture(&postitnoir);
                        }
                        if (j -> input[8-1][0] == 'b') {
                            postit8.setTexture(&postitbleu);
                        }
                        if (j -> input[8-1][0] == 'r') {
                            postit8.setTexture(&postitrouge);
                        }
                    } else {
                        postit8.setTexture(&postitblanc);
                    }

                        // 9
                    if (j -> input[9-1][1] == 'v') {
                        if (j -> input[9-1][0] == 'w') {
                            postit9.setTexture(&postitneutre);
                        }
                        if (j -> input[9-1][0] == 'n') {
                            postit9.setTexture(&postitnoir);
                        }
                        if (j -> input[9-1][0] == 'b') {
                            postit9.setTexture(&postitbleu);
                        }
                        if (j -> input[9-1][0] == 'r') {
                            postit9.setTexture(&postitrouge);
                        }
                    } else {
                        postit9.setTexture(&postitblanc);
                    }

                        // 10
                    if (j -> input[10-1][1] == 'v') {
                        if (j -> input[10-1][0] == 'w') {
                            postit10.setTexture(&postitneutre);
                        }
                        if (j -> input[10-1][0] == 'n') {
                            postit10.setTexture(&postitnoir);
                        }
                        if (j -> input[10-1][0] == 'b') {
                            postit10.setTexture(&postitbleu);
                        }
                        if (j -> input[10-1][0] == 'r') {
                            postit10.setTexture(&postitrouge);
                        }
                    } else {
                        postit10.setTexture(&postitblanc);
                    }

                        // 11
                    if (j -> input[11-1][1] == 'v') {
                        if (j -> input[11-1][0] == 'w') {
                            postit11.setTexture(&postitneutre);
                        }
                        if (j -> input[11-1][0] == 'n') {
                            postit11.setTexture(&postitnoir);
                        }
                        if (j -> input[11-1][0] == 'b') {
                            postit11.setTexture(&postitbleu);
                        }
                        if (j -> input[11-1][0] == 'r') {
                            postit11.setTexture(&postitrouge);
                        }
                    } else {
                        postit11.setTexture(&postitblanc);
                    }

                        // 12
                    if (j -> input[12-1][1] == 'v') {
                        if (j -> input[12-1][0] == 'w') {
                            postit12.setTexture(&postitneutre);
                        }
                        if (j -> input[12-1][0] == 'n') {
                            postit12.setTexture(&postitnoir);
                        }
                        if (j -> input[12-1][0] == 'b') {
                            postit12.setTexture(&postitbleu);
                        }
                        if (j -> input[12-1][0] == 'r') {
                            postit12.setTexture(&postitrouge);
                        }
                    } else {
                        postit12.setTexture(&postitblanc);
                    }

                        // 13
                    if (j -> input[13-1][1] == 'v') {
                        if (j -> input[13-1][0] == 'w') {
                            postit13.setTexture(&postitneutre);
                        }
                        if (j -> input[13-1][0] == 'n') {
                            postit13.setTexture(&postitnoir);
                        }
                        if (j -> input[13-1][0] == 'b') {
                            postit13.setTexture(&postitbleu);
                        }
                        if (j -> input[13-1][0] == 'r') {
                            postit13.setTexture(&postitrouge);
                        }
                    } else {
                        postit13.setTexture(&postitblanc);
                    }

                        // 14
                    if (j -> input[14-1][1] == 'v') {
                        if (j -> input[14-1][0] == 'w') {
                            postit14.setTexture(&postitneutre);
                        }
                        if (j -> input[14-1][0] == 'n') {
                            postit14.setTexture(&postitnoir);
                        }
                        if (j -> input[14-1][0] == 'b') {
                            postit14.setTexture(&postitbleu);
                        }
                        if (j -> input[14-1][0] == 'r') {
                            postit14.setTexture(&postitrouge);
                        }
                    } else {
                        postit14.setTexture(&postitblanc);
                    }

                        // 15
                    if (j -> input[15-1][1] == 'v') {
                        if (j -> input[15-1][0] == 'w') {
                            postit15.setTexture(&postitneutre);
                        }
                        if (j -> input[15-1][0] == 'n') {
                            postit15.setTexture(&postitnoir);
                        }
                        if (j -> input[15-1][0] == 'b') {
                            postit15.setTexture(&postitbleu);
                        }
                        if (j -> input[15-1][0] == 'r') {
                            postit15.setTexture(&postitrouge);
                        }
                    } else {
                        postit15.setTexture(&postitblanc);
                    }

                        // 16
                    if (j -> input[16-1][1] == 'v') {
                        if (j -> input[16-1][0] == 'w') {
                            postit16.setTexture(&postitneutre);
                        }
                        if (j -> input[16-1][0] == 'n') {
                            postit16.setTexture(&postitnoir);
                        }
                        if (j -> input[16-1][0] == 'b') {
                            postit16.setTexture(&postitbleu);
                        }
                        if (j -> input[16-1][0] == 'r') {
                            postit16.setTexture(&postitrouge);
                        }
                    } else {
                        postit16.setTexture(&postitblanc);
                    }

                        // 17
                    if (j -> input[17-1][1] == 'v') {
                        if (j -> input[17-1][0] == 'w') {
                            postit17.setTexture(&postitneutre);
                        }
                        if (j -> input[17-1][0] == 'n') {
                            postit17.setTexture(&postitnoir);
                        }
                        if (j -> input[17-1][0] == 'b') {
                            postit17.setTexture(&postitbleu);
                        }
                        if (j -> input[17-1][0] == 'r') {
                            postit17.setTexture(&postitrouge);
                        }
                    } else {
                        postit17.setTexture(&postitblanc);
                    }

                        // 18
                    if (j -> input[18-1][1] == 'v') {
                        if (j -> input[18-1][0] == 'w') {
                            postit18.setTexture(&postitneutre);
                        }
                        if (j -> input[18-1][0] == 'n') {
                            postit18.setTexture(&postitnoir);
                        }
                        if (j -> input[18-1][0] == 'b') {
                            postit18.setTexture(&postitbleu);
                        }
                        if (j -> input[18-1][0] == 'r') {
                            postit18.setTexture(&postitrouge);
                        }
                    } else {
                        postit18.setTexture(&postitblanc);
                    }

                        // 19
                    if (j -> input[19-1][1] == 'v') {
                        if (j -> input[19-1][0] == 'w') {
                            postit19.setTexture(&postitneutre);
                        }
                        if (j -> input[19-1][0] == 'n') {
                            postit19.setTexture(&postitnoir);
                        }
                        if (j -> input[19-1][0] == 'b') {
                            postit19.setTexture(&postitbleu);
                        }
                        if (j -> input[19-1][0] == 'r') {
                            postit19.setTexture(&postitrouge);
                        }
                    } else {
                        postit19.setTexture(&postitblanc);
                    }

                        // 20
                    if (j -> input[20-1][1] == 'v') {
                        if (j -> input[20-1][0] == 'w') {
                            postit20.setTexture(&postitneutre);
                        }
                        if (j -> input[20-1][0] == 'n') {
                            postit20.setTexture(&postitnoir);
                        }
                        if (j -> input[20-1][0] == 'b') {
                            postit20.setTexture(&postitbleu);
                        }
                        if (j -> input[20-1][0] == 'r') {
                            postit20.setTexture(&postitrouge);
                        }
                    } else {
                        postit20.setTexture(&postitblanc);
                    }

                        // 21
                    if (j -> input[21-1][1] == 'v') {
                        if (j -> input[21-1][0] == 'w') {
                            postit21.setTexture(&postitneutre);
                        }
                        if (j -> input[21-1][0] == 'n') {
                            postit21.setTexture(&postitnoir);
                        }
                        if (j -> input[21-1][0] == 'b') {
                            postit21.setTexture(&postitbleu);
                        }
                        if (j -> input[21-1][0] == 'r') {
                            postit21.setTexture(&postitrouge);
                        }
                    } else {
                        postit21.setTexture(&postitblanc);
                    }

                        // 22
                    if (j -> input[22-1][1] == 'v') {
                        if (j -> input[22-1][0] == 'w') {
                            postit22.setTexture(&postitneutre);
                        }
                        if (j -> input[22-1][0] == 'n') {
                            postit22.setTexture(&postitnoir);
                        }
                        if (j -> input[22-1][0] == 'b') {
                            postit22.setTexture(&postitbleu);
                        }
                        if (j -> input[22-1][0] == 'r') {
                            postit22.setTexture(&postitrouge);
                        }
                    } else {
                        postit22.setTexture(&postitblanc);
                    }

                        // 23
                    if (j -> input[23-1][1] == 'v') {
                        if (j -> input[23-1][0] == 'w') {
                            postit23.setTexture(&postitneutre);
                        }
                        if (j -> input[23-1][0] == 'n') {
                            postit23.setTexture(&postitnoir);
                        }
                        if (j -> input[23-1][0] == 'b') {
                            postit23.setTexture(&postitbleu);
                        }
                        if (j -> input[23-1][0] == 'r') {
                            postit23.setTexture(&postitrouge);
                        }
                    } else {
                        postit23.setTexture(&postitblanc);
                    }

                        // 24
                    if (j -> input[24-1][1] == 'v') {
                        if (j -> input[24-1][0] == 'w') {
                            postit24.setTexture(&postitneutre);
                        }
                        if (j -> input[24-1][0] == 'n') {
                            postit24.setTexture(&postitnoir);
                        }
                        if (j -> input[24-1][0] == 'b') {
                            postit24.setTexture(&postitbleu);
                        }
                        if (j -> input[24-1][0] == 'r') {
                            postit24.setTexture(&postitrouge);
                        }
                    } else {
                        postit24.setTexture(&postitblanc);
                    }

                        // 25
                    if (j -> input[25-1][1] == 'v') {
                        if (j -> input[25-1][0] == 'w') {
                            postit25.setTexture(&postitneutre);
                        }
                        if (j -> input[25-1][0] == 'n') {
                            postit25.setTexture(&postitnoir);
                        }
                        if (j -> input[25-1][0] == 'b') {
                            postit25.setTexture(&postitbleu);
                        }
                        if (j -> input[25-1][0] == 'r') {
                            postit25.setTexture(&postitrouge);
                        }
                    } else {
                        postit25.setTexture(&postitblanc);
                    }




                    sf::Event event;
                    while (game.pollEvent(event))
                    {
                        if (event.type == sf::Event::Closed)
                            game.close();
                    }


                        // On affiche tous les éléments
                    game.clear();
                    if (j -> equipetour == 'b') {
                        game.draw(fondbleu);
                    } else {
                        game.draw(fondrouge);
                    }
                    game.draw(postit1);
                    game.draw(postittext1);
                    game.draw(postit2);
                    game.draw(postittext2);
                    game.draw(postit3);
                    game.draw(postittext3);
                    game.draw(postit4);
                    game.draw(postittext4);
                    game.draw(postit5);
                    game.draw(postittext5);
                    game.draw(postit6);
                    game.draw(postittext6);
                    game.draw(postit7);
                    game.draw(postittext7);
                    game.draw(postit8);
                    game.draw(postittext8);
                    game.draw(postit9);
                    game.draw(postittext9);
                    game.draw(postit10);
                    game.draw(postittext10);
                    game.draw(postit11);
                    game.draw(postittext11);
                    game.draw(postit12);
                    game.draw(postittext12);
                    game.draw(postit13);
                    game.draw(postittext13);
                    game.draw(postit14);
                    game.draw(postittext14);
                    game.draw(postit15);
                    game.draw(postittext15);
                    game.draw(postit16);
                    game.draw(postittext16);
                    game.draw(postit17);
                    game.draw(postittext17);
                    game.draw(postit18);
                    game.draw(postittext18);
                    game.draw(postit19);
                    game.draw(postittext19);
                    game.draw(postit20);
                    game.draw(postittext20);
                    game.draw(postit21);
                    game.draw(postittext21);
                    game.draw(postit22);
                    game.draw(postittext22);
                    game.draw(postit23);
                    game.draw(postittext23);
                    game.draw(postit24);
                    game.draw(postittext24);
                    game.draw(postit25);
                    game.draw(postittext25);
                    game.draw(interrupteur1);
                    game.display();

                    if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
                    {
                            // RECUPERE LA POSITION DE LA SOURIS AU MOMENT DU CLIC
                        sf::Vector2f mouse = game.mapPixelToCoords(sf::Mouse::getPosition(game));

                            // RECUPERE LA POSITION DES BOUTTONS
                        sf::FloatRect locationpostit1 = postit1.getGlobalBounds();
                        sf::FloatRect locationpostit2 = postit2.getGlobalBounds();
                        sf::FloatRect locationpostit3 = postit3.getGlobalBounds();
                        sf::FloatRect locationpostit4 = postit4.getGlobalBounds();
                        sf::FloatRect locationpostit5 = postit5.getGlobalBounds();
                        sf::FloatRect locationpostit6 = postit6.getGlobalBounds();
                        sf::FloatRect locationpostit7 = postit7.getGlobalBounds();
                        sf::FloatRect locationpostit8 = postit8.getGlobalBounds();
                        sf::FloatRect locationpostit9 = postit9.getGlobalBounds();
                        sf::FloatRect locationpostit10 = postit10.getGlobalBounds();
                        sf::FloatRect locationpostit11 = postit11.getGlobalBounds();
                        sf::FloatRect locationpostit12 = postit12.getGlobalBounds();
                        sf::FloatRect locationpostit13 = postit13.getGlobalBounds();
                        sf::FloatRect locationpostit14 = postit14.getGlobalBounds();
                        sf::FloatRect locationpostit15 = postit15.getGlobalBounds();
                        sf::FloatRect locationpostit16 = postit16.getGlobalBounds();
                        sf::FloatRect locationpostit17 = postit17.getGlobalBounds();
                        sf::FloatRect locationpostit18 = postit18.getGlobalBounds();
                        sf::FloatRect locationpostit19 = postit19.getGlobalBounds();
                        sf::FloatRect locationpostit20 = postit20.getGlobalBounds();
                        sf::FloatRect locationpostit21 = postit21.getGlobalBounds();
                        sf::FloatRect locationpostit22 = postit22.getGlobalBounds();
                        sf::FloatRect locationpostit23 = postit23.getGlobalBounds();
                        sf::FloatRect locationpostit24 = postit24.getGlobalBounds();
                        sf::FloatRect locationpostit25 = postit25.getGlobalBounds();

                        sf::FloatRect interrupteur = interrupteur1.getGlobalBounds();

                        // On vérifie si le jeu n'est pas over
                        if (j->equipegagne == 'b') {
                            sf::Texture background;
                            if (!background.loadFromFile(resourcePath() + "Victoirebleu.png"))
                                return EXIT_FAILURE;
                            sf::Sprite back;
                            sf::Vector2u TextureSize;
                            sf::Vector2u WindowSize;
                            TextureSize = background.getSize();
                            WindowSize = game.getSize();
                            float ScaleX = (float)WindowSize.x / TextureSize.x;
                            float ScaleY = (float)WindowSize.y / TextureSize.y;
                            back.setTexture(background);
                            back.setScale(ScaleX, ScaleY);
                            game.clear();
                            game.draw(back);
                            game.display();
                            sleep(10);
                            game.close();
                        }

                        if (j->equipegagne == 'r') {
                            sf::Texture background;
                            if (!background.loadFromFile(resourcePath() + "Victoirerouge.png"))
                                return EXIT_FAILURE;
                            sf::Sprite back;
                            sf::Vector2u TextureSize;
                            sf::Vector2u WindowSize;
                            TextureSize = background.getSize();
                            WindowSize = game.getSize();
                            float ScaleX = (float)WindowSize.x / TextureSize.x;
                            float ScaleY = (float)WindowSize.y / TextureSize.y;
                            back.setTexture(background);
                            back.setScale(ScaleX, ScaleY);
                            game.clear();
                            game.draw(back);
                            game.display();
                            sleep(10);
                            game.close();
                        }

                        if (j->equipegagne == '2') {
                            sf::Texture background;
                            if (!background.loadFromFile(resourcePath() + "Matchnul.png"))
                                return EXIT_FAILURE;
                            sf::Sprite back;
                            sf::Vector2u TextureSize;
                            sf::Vector2u WindowSize;
                            TextureSize = background.getSize();
                            WindowSize = game.getSize();
                            float ScaleX = (float)WindowSize.x / TextureSize.x;
                            float ScaleY = (float)WindowSize.y / TextureSize.y;
                            back.setTexture(background);
                            back.setScale(ScaleX, ScaleY);
                            game.clear();
                            game.draw(back);
                            game.display();
                            sleep(10);
                            game.close();
                        }

                        if (locationpostit1.contains(mouse))
                        {

                            j -> casesjouer = 1-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit2.contains(mouse))
                        {

                            j -> casesjouer = 2-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit3.contains(mouse))
                        {

                            j -> casesjouer = 3-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit4.contains(mouse))
                        {

                            j -> casesjouer = 4-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit5.contains(mouse))
                        {

                            j -> casesjouer = 5-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit6.contains(mouse))
                        {

                            j -> casesjouer = 6-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit7.contains(mouse))
                        {

                            j -> casesjouer = 7-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit8.contains(mouse))
                        {

                            j -> casesjouer = 8-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit9.contains(mouse))
                        {

                            j -> casesjouer = 9-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit10.contains(mouse))
                        {

                            j -> casesjouer = 10-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit11.contains(mouse))
                        {

                            j -> casesjouer = 11-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit12.contains(mouse))
                        {

                            j -> casesjouer = 12-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit13.contains(mouse))
                        {

                            j -> casesjouer = 13-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit14.contains(mouse))
                        {

                            j -> casesjouer = 14-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit15.contains(mouse))
                        {

                            j -> casesjouer = 15-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit16.contains(mouse))
                        {

                            j -> casesjouer = 16-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit17.contains(mouse))
                        {

                            j -> casesjouer = 17-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit18.contains(mouse))
                        {

                            j -> casesjouer = 18-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit19.contains(mouse))
                        {

                            j -> casesjouer = 19-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit20.contains(mouse))
                        {

                            j -> casesjouer = 20-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit21.contains(mouse))
                        {

                            j -> casesjouer = 21-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit22.contains(mouse))
                        {

                            j -> casesjouer = 22-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit23.contains(mouse))
                        {

                            j -> casesjouer = 23-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit24.contains(mouse))
                        {

                            j -> casesjouer = 24-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }

                        if (locationpostit25.contains(mouse))
                        {

                            j -> casesjouer = 25-1;
                            j = jouer(j);
                            j = verifie_gameover(j);
                            sleep(1);
                        }


                        if (interrupteur.contains(mouse))
                        {
                            sf::RenderWindow game(sf::VideoMode(960, 540), "Maitre Espion");
                            game.setVerticalSyncEnabled(true);

                            sf::Texture fond;
                            if (!fond.loadFromFile(resourcePath() + "Plateauvidesombre.png"))
                                  return EXIT_FAILURE;

                            sf::Sprite fondsombre;
                            sf::Vector2u TextureSize;
                            sf::Vector2u WindowSize;
                            TextureSize = fond.getSize();
                            WindowSize = menu.getSize();
                            float ScaleX = (float)WindowSize.x / TextureSize.x;
                            float ScaleY = (float)WindowSize.y / TextureSize.y;
                            fondsombre.setTexture(fond);
                            fondsombre.setScale(ScaleX, ScaleY);


                            sf::Texture postitblanc;
                            if (!postitblanc.loadFromFile(resourcePath() + "Postitblanc.png"))
                            {
                                return EXIT_FAILURE;
                            }

                            sf::Texture postitbleu;
                            if (!postitbleu.loadFromFile(resourcePath() + "Postitbleu.png"))
                            {
                                return EXIT_FAILURE;
                            }

                            sf::Texture postitrouge;
                            if (!postitrouge.loadFromFile(resourcePath() + "Postitrouge.png"))
                            {
                                return EXIT_FAILURE;
                            }

                            sf::Texture postitnoir;
                            if (!postitnoir.loadFromFile(resourcePath() + "Postitnoir.png"))
                            {
                                return EXIT_FAILURE;
                            }

                            sf::Texture postitneutre;
                            if (!postitneutre.loadFromFile(resourcePath() + "Postitneutre.png"))
                            {
                                return EXIT_FAILURE;
                            }

                            sf::Font font;
                            if (!font.loadFromFile(resourcePath() + "Caveat.ttf"))
                            {
                                return EXIT_FAILURE;
                            }


                                // On crée les 25 post-it

                                // 1
                            sf::RectangleShape postit1;
                            postit1.setSize(sf::Vector2f(100, 90));
                            postit1.setPosition(sf::Vector2f(50, 40));
                            postit1.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext1;
                            postittext1.setFont(font);
                            postittext1.setString(stringToWstring(j -> mots[1-1]));
                            postittext1.setCharacterSize(22);
                            postittext1.setFillColor(sf::Color::Black);
                            postittext1.setPosition(sf::Vector2f(60, 65));

                                // 2
                            sf::RectangleShape postit2;
                            postit2.setSize(sf::Vector2f(100, 90));
                            postit2.setPosition(sf::Vector2f(160, 40));
                            postit2.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext2;
                            postittext2.setFont(font);
                            postittext2.setString(stringToWstring(j -> mots[2-1]));
                            postittext2.setCharacterSize(22);
                            postittext2.setFillColor(sf::Color::Black);
                            postittext2.setPosition(sf::Vector2f(170, 65));

                                // 3
                            sf::RectangleShape postit3;
                            postit3.setSize(sf::Vector2f(100, 90));
                            postit3.setPosition(sf::Vector2f(270, 40));
                            postit3.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext3;
                            postittext3.setFont(font);
                            postittext3.setString(stringToWstring(j -> mots[3-1]));
                            postittext3.setCharacterSize(22);
                            postittext3.setFillColor(sf::Color::Black);
                            postittext3.setPosition(sf::Vector2f(280, 65));

                                // 4
                            sf::RectangleShape postit4;
                            postit4.setSize(sf::Vector2f(100, 90));
                            postit4.setPosition(sf::Vector2f(380, 40));
                            postit4.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext4;
                            postittext4.setFont(font);
                            postittext4.setString(stringToWstring(j -> mots[4-1]));
                            postittext4.setCharacterSize(22);
                            postittext4.setFillColor(sf::Color::Black);
                            postittext4.setPosition(sf::Vector2f(390, 65));

                                // 5
                            sf::RectangleShape postit5;
                            postit5.setSize(sf::Vector2f(100, 90));
                            postit5.setPosition(sf::Vector2f(490, 40));
                            postit5.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext5;
                            postittext5.setFont(font);
                            postittext5.setString(stringToWstring(j -> mots[5-1]));
                            postittext5.setCharacterSize(22);
                            postittext5.setFillColor(sf::Color::Black);
                            postittext5.setPosition(sf::Vector2f(500, 65));

                                // 6
                            sf::RectangleShape postit6;
                            postit6.setSize(sf::Vector2f(100, 90));
                            postit6.setPosition(sf::Vector2f(50, 135));
                            postit6.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext6;
                            postittext6.setFont(font);
                            postittext6.setString(stringToWstring(j -> mots[6-1]));
                            postittext6.setCharacterSize(22);
                            postittext6.setFillColor(sf::Color::Black);
                            postittext6.setPosition(sf::Vector2f(60, 160));

                                // 7
                            sf::RectangleShape postit7;
                            postit7.setSize(sf::Vector2f(100, 90));
                            postit7.setPosition(sf::Vector2f(160, 135));
                            postit7.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext7;
                            postittext7.setFont(font);
                            postittext7.setString(stringToWstring(j -> mots[7-1]));
                            postittext7.setCharacterSize(22);
                            postittext7.setFillColor(sf::Color::Black);
                            postittext7.setPosition(sf::Vector2f(170, 160));

                                // 8
                            sf::RectangleShape postit8;
                            postit8.setSize(sf::Vector2f(100, 90));
                            postit8.setPosition(sf::Vector2f(270, 135));
                            postit8.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext8;
                            postittext8.setFont(font);
                            postittext8.setString(stringToWstring(j -> mots[8-1]));
                            postittext8.setCharacterSize(22);
                            postittext8.setFillColor(sf::Color::Black);
                            postittext8.setPosition(sf::Vector2f(280, 160));

                                // 9
                            sf::RectangleShape postit9;
                            postit9.setSize(sf::Vector2f(100, 90));
                            postit9.setPosition(sf::Vector2f(380, 135));
                            postit9.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext9;
                            postittext9.setFont(font);
                            postittext9.setString(stringToWstring(j -> mots[9-1]));
                            postittext9.setCharacterSize(22);
                            postittext9.setFillColor(sf::Color::Black);
                            postittext9.setPosition(sf::Vector2f(390, 160));

                                // 10
                            sf::RectangleShape postit10;
                            postit10.setSize(sf::Vector2f(100, 90));
                            postit10.setPosition(sf::Vector2f(490, 135));
                            postit10.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext10;
                            postittext10.setFont(font);
                            postittext10.setString(stringToWstring(j -> mots[10-1]));
                            postittext10.setCharacterSize(22);
                            postittext10.setFillColor(sf::Color::Black);
                            postittext10.setPosition(sf::Vector2f(500, 160));

                                // 11
                            sf::RectangleShape postit11;
                            postit11.setSize(sf::Vector2f(100, 90));
                            postit11.setPosition(sf::Vector2f(50, 230));
                            postit11.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext11;
                            postittext11.setFont(font);
                            postittext11.setString(stringToWstring(j -> mots[11-1]));
                            postittext11.setCharacterSize(22);
                            postittext11.setFillColor(sf::Color::Black);
                            postittext11.setPosition(sf::Vector2f(60, 255));

                                // 12
                            sf::RectangleShape postit12;
                            postit12.setSize(sf::Vector2f(100, 90));
                            postit12.setPosition(sf::Vector2f(160, 230));
                            postit12.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext12;
                            postittext12.setFont(font);
                            postittext12.setString(stringToWstring(j -> mots[12-1]));
                            postittext12.setCharacterSize(22);
                            postittext12.setFillColor(sf::Color::Black);
                            postittext12.setPosition(sf::Vector2f(170, 255));

                                // 13
                            sf::RectangleShape postit13;
                            postit13.setSize(sf::Vector2f(100, 90));
                            postit13.setPosition(sf::Vector2f(270, 230));
                            postit13.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext13;
                            postittext13.setFont(font);
                            postittext13.setString(stringToWstring(j -> mots[13-1]));
                            postittext13.setCharacterSize(22);
                            postittext13.setFillColor(sf::Color::Black);
                            postittext13.setPosition(sf::Vector2f(280, 255));

                                // 14
                            sf::RectangleShape postit14;
                            postit14.setSize(sf::Vector2f(100, 90));
                            postit14.setPosition(sf::Vector2f(380, 230));
                            postit14.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext14;
                            postittext14.setFont(font);
                            postittext14.setString(stringToWstring(j -> mots[14-1]));
                            postittext14.setCharacterSize(22);
                            postittext14.setFillColor(sf::Color::Black);
                            postittext14.setPosition(sf::Vector2f(390, 255));

                                // 15
                            sf::RectangleShape postit15;
                            postit15.setSize(sf::Vector2f(100, 90));
                            postit15.setPosition(sf::Vector2f(490, 230));
                            postit15.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext15;
                            postittext15.setFont(font);
                            postittext15.setString(stringToWstring(j -> mots[15-1]));
                            postittext15.setCharacterSize(22);
                            postittext15.setFillColor(sf::Color::Black);
                            postittext15.setPosition(sf::Vector2f(500, 255));

                                // 16
                            sf::RectangleShape postit16;
                            postit16.setSize(sf::Vector2f(100, 90));
                            postit16.setPosition(sf::Vector2f(50, 325));
                            postit16.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext16;
                            postittext16.setFont(font);
                            postittext16.setString(stringToWstring(j -> mots[16-1]));
                            postittext16.setCharacterSize(22);
                            postittext16.setFillColor(sf::Color::Black);
                            postittext16.setPosition(sf::Vector2f(60, 350));

                                // 17
                            sf::RectangleShape postit17;
                            postit17.setSize(sf::Vector2f(100, 90));
                            postit17.setPosition(sf::Vector2f(160, 325));
                            postit17.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext17;
                            postittext17.setFont(font);
                            postittext17.setString(stringToWstring(j -> mots[17-1]));
                            postittext17.setCharacterSize(22);
                            postittext17.setFillColor(sf::Color::Black);
                            postittext17.setPosition(sf::Vector2f(170, 350));

                                // 18
                            sf::RectangleShape postit18;
                            postit18.setSize(sf::Vector2f(100, 90));
                            postit18.setPosition(sf::Vector2f(270, 325));
                            postit18.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext18;
                            postittext18.setFont(font);
                            postittext18.setString(stringToWstring(j -> mots[18-1]));
                            postittext18.setCharacterSize(22);
                            postittext18.setFillColor(sf::Color::Black);
                            postittext18.setPosition(sf::Vector2f(280, 350));

                                // 19
                            sf::RectangleShape postit19;
                            postit19.setSize(sf::Vector2f(100, 90));
                            postit19.setPosition(sf::Vector2f(380, 325));
                            postit19.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext19;
                            postittext19.setFont(font);
                            postittext19.setString(stringToWstring(j -> mots[19-1]));
                            postittext19.setCharacterSize(22);
                            postittext19.setFillColor(sf::Color::Black);
                            postittext19.setPosition(sf::Vector2f(390, 350));

                                // 20
                            sf::RectangleShape postit20;
                            postit20.setSize(sf::Vector2f(100, 90));
                            postit20.setPosition(sf::Vector2f(490, 325));
                            postit20.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext20;
                            postittext20.setFont(font);
                            postittext20.setString(stringToWstring(j -> mots[20-1]));
                            postittext20.setCharacterSize(22);
                            postittext20.setFillColor(sf::Color::Black);
                            postittext20.setPosition(sf::Vector2f(500, 350));

                                // 21
                            sf::RectangleShape postit21;
                            postit21.setSize(sf::Vector2f(100, 90));
                            postit21.setPosition(sf::Vector2f(50, 420));
                            postit21.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext21;
                            postittext21.setFont(font);
                            postittext21.setString(stringToWstring(j -> mots[21-1]));
                            postittext21.setCharacterSize(22);
                            postittext21.setFillColor(sf::Color::Black);
                            postittext21.setPosition(sf::Vector2f(60, 445));

                                // 22
                            sf::RectangleShape postit22;
                            postit22.setSize(sf::Vector2f(100, 90));
                            postit22.setPosition(sf::Vector2f(160, 420));
                            postit22.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext22;
                            postittext22.setFont(font);
                            postittext22.setString(stringToWstring(j -> mots[22-1]));
                            postittext22.setCharacterSize(22);
                            postittext22.setFillColor(sf::Color::Black);
                            postittext22.setPosition(sf::Vector2f(170, 445));

                                // 23
                            sf::RectangleShape postit23;
                            postit23.setSize(sf::Vector2f(100, 90));
                            postit23.setPosition(sf::Vector2f(270, 420));
                            postit23.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext23;
                            postittext23.setFont(font);
                            postittext23.setString(stringToWstring(j -> mots[23-1]));
                            postittext23.setCharacterSize(22);
                            postittext23.setFillColor(sf::Color::Black);
                            postittext23.setPosition(sf::Vector2f(280, 445));

                                // 24
                            sf::RectangleShape postit24;
                            postit24.setSize(sf::Vector2f(100, 90));
                            postit24.setPosition(sf::Vector2f(380, 420));
                            postit24.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext24;
                            postittext24.setFont(font);
                            postittext24.setString(stringToWstring(j -> mots[24-1]));
                            postittext24.setCharacterSize(22);
                            postittext24.setFillColor(sf::Color::Black);
                            postittext24.setPosition(sf::Vector2f(390, 445));

                                // 25
                            sf::RectangleShape postit25;
                            postit25.setSize(sf::Vector2f(100, 90));
                            postit25.setPosition(sf::Vector2f(490, 420));
                            postit25.setRotation((fonct_rand()%3) - (fonct_rand()%3));

                            sf::Text postittext25;
                            postittext25.setFont(font);
                            postittext25.setString(stringToWstring(j -> mots[25-1]));
                            postittext25.setCharacterSize(22);
                            postittext25.setFillColor(sf::Color::Black);
                            postittext25.setPosition(sf::Vector2f(500, 445));


                                // On crée l'interrupteur
                            sf::Texture interrupteuron;
                            if (!interrupteuron.loadFromFile(resourcePath() + "Interrupteuron.png"))
                            {
                                return EXIT_FAILURE;
                            }

                            sf::RectangleShape interrupteur2;
                            interrupteur2.setSize(sf::Vector2f(110, 166));
                            interrupteur2.setPosition(sf::Vector2f(670, 340));
                            interrupteur2.setTexture(&interrupteuron);


                            while (game.isOpen())
                            {

                                    // On gère la couleur des post-it

                                    // 1
                                if (j -> input[1-1][0] == 'w') {
                                    postit1.setTexture(&postitneutre);
                                }
                                if (j -> input[1-1][0] == 'n') {
                                    postit1.setTexture(&postitnoir);
                                }
                                if (j -> input[1-1][0] == 'b') {
                                    postit1.setTexture(&postitbleu);
                                }
                                if (j -> input[1-1][0] == 'r') {
                                    postit1.setTexture(&postitrouge);
                                }

                                    // 2
                                if (j -> input[2-1][0] == 'w') {
                                    postit2.setTexture(&postitneutre);
                                }
                                if (j -> input[2-1][0] == 'n') {
                                    postit2.setTexture(&postitnoir);
                                }
                                if (j -> input[2-1][0] == 'b') {
                                    postit2.setTexture(&postitbleu);
                                }
                                if (j -> input[2-1][0] == 'r') {
                                    postit2.setTexture(&postitrouge);
                                }


                                    // 3
                                if (j -> input[3-1][0] == 'w') {
                                    postit3.setTexture(&postitneutre);
                                }
                                if (j -> input[3-1][0] == 'n') {
                                    postit3.setTexture(&postitnoir);
                                }
                                if (j -> input[3-1][0] == 'b') {
                                    postit3.setTexture(&postitbleu);
                                }
                                if (j -> input[3-1][0] == 'r') {
                                    postit3.setTexture(&postitrouge);
                                }


                                    // 4
                                if (j -> input[4-1][0] == 'w') {
                                    postit4.setTexture(&postitneutre);
                                }
                                if (j -> input[4-1][0] == 'n') {
                                    postit4.setTexture(&postitnoir);
                                }
                                if (j -> input[4-1][0] == 'b') {
                                    postit4.setTexture(&postitbleu);
                                }
                                if (j -> input[4-1][0] == 'r') {
                                    postit4.setTexture(&postitrouge);
                                }


                                    // 5
                                if (j -> input[5-1][0] == 'w') {
                                    postit5.setTexture(&postitneutre);
                                }
                                if (j -> input[5-1][0] == 'n') {
                                    postit5.setTexture(&postitnoir);
                                }
                                if (j -> input[5-1][0] == 'b') {
                                    postit5.setTexture(&postitbleu);
                                }
                                if (j -> input[5-1][0] == 'r') {
                                    postit5.setTexture(&postitrouge);
                                }


                                    // 6

                                if (j -> input[6-1][0] == 'w') {
                                    postit6.setTexture(&postitneutre);
                                }
                                if (j -> input[6-1][0] == 'n') {
                                    postit6.setTexture(&postitnoir);
                                }
                                if (j -> input[6-1][0] == 'b') {
                                    postit6.setTexture(&postitbleu);
                                }
                                if (j -> input[6-1][0] == 'r') {
                                    postit6.setTexture(&postitrouge);
                                }


                                    // 7
                                if (j -> input[7-1][0] == 'w') {
                                    postit7.setTexture(&postitneutre);
                                }
                                if (j -> input[7-1][0] == 'n') {
                                    postit7.setTexture(&postitnoir);
                                }
                                if (j -> input[7-1][0] == 'b') {
                                    postit7.setTexture(&postitbleu);
                                }
                                if (j -> input[7-1][0] == 'r') {
                                    postit7.setTexture(&postitrouge);
                                }


                                    // 8
                                if (j -> input[8-1][0] == 'w') {
                                    postit8.setTexture(&postitneutre);
                                }
                                if (j -> input[8-1][0] == 'n') {
                                    postit8.setTexture(&postitnoir);
                                }
                                if (j -> input[8-1][0] == 'b') {
                                    postit8.setTexture(&postitbleu);
                                }
                                if (j -> input[8-1][0] == 'r') {
                                    postit8.setTexture(&postitrouge);
                                }


                                    // 9
                                if (j -> input[9-1][0] == 'w') {
                                    postit9.setTexture(&postitneutre);
                                }
                                if (j -> input[9-1][0] == 'n') {
                                    postit9.setTexture(&postitnoir);
                                }
                                if (j -> input[9-1][0] == 'b') {
                                    postit9.setTexture(&postitbleu);
                                }
                                if (j -> input[9-1][0] == 'r') {
                                    postit9.setTexture(&postitrouge);
                                }


                                    // 10
                                if (j -> input[10-1][0] == 'w') {
                                    postit10.setTexture(&postitneutre);
                                }
                                if (j -> input[10-1][0] == 'n') {
                                    postit10.setTexture(&postitnoir);
                                }
                                if (j -> input[10-1][0] == 'b') {
                                    postit10.setTexture(&postitbleu);
                                }
                                if (j -> input[10-1][0] == 'r') {
                                    postit10.setTexture(&postitrouge);
                                }


                                    // 11
                                if (j -> input[11-1][0] == 'w') {
                                    postit11.setTexture(&postitneutre);
                                }
                                if (j -> input[11-1][0] == 'n') {
                                    postit11.setTexture(&postitnoir);
                                }
                                if (j -> input[11-1][0] == 'b') {
                                    postit11.setTexture(&postitbleu);
                                }
                                if (j -> input[11-1][0] == 'r') {
                                    postit11.setTexture(&postitrouge);
                                }


                                    // 12
                                if (j -> input[12-1][0] == 'w') {
                                    postit12.setTexture(&postitneutre);
                                }
                                if (j -> input[12-1][0] == 'n') {
                                    postit12.setTexture(&postitnoir);
                                }
                                if (j -> input[12-1][0] == 'b') {
                                    postit12.setTexture(&postitbleu);
                                }
                                if (j -> input[12-1][0] == 'r') {
                                    postit12.setTexture(&postitrouge);
                                }


                                    // 13
                                if (j -> input[13-1][0] == 'w') {
                                    postit13.setTexture(&postitneutre);
                                }
                                if (j -> input[13-1][0] == 'n') {
                                    postit13.setTexture(&postitnoir);
                                }
                                if (j -> input[13-1][0] == 'b') {
                                    postit13.setTexture(&postitbleu);
                                }
                                if (j -> input[13-1][0] == 'r') {
                                    postit13.setTexture(&postitrouge);
                                }


                                    // 14
                                if (j -> input[14-1][0] == 'w') {
                                    postit14.setTexture(&postitneutre);
                                }
                                if (j -> input[14-1][0] == 'n') {
                                    postit14.setTexture(&postitnoir);
                                }
                                if (j -> input[14-1][0] == 'b') {
                                    postit14.setTexture(&postitbleu);
                                }
                                if (j -> input[14-1][0] == 'r') {
                                    postit14.setTexture(&postitrouge);
                                }


                                    // 15
                                if (j -> input[15-1][0] == 'w') {
                                    postit15.setTexture(&postitneutre);
                                }
                                if (j -> input[15-1][0] == 'n') {
                                    postit15.setTexture(&postitnoir);
                                }
                                if (j -> input[15-1][0] == 'b') {
                                    postit15.setTexture(&postitbleu);
                                }
                                if (j -> input[15-1][0] == 'r') {
                                    postit15.setTexture(&postitrouge);
                                }


                                    // 16
                                if (j -> input[16-1][0] == 'w') {
                                    postit16.setTexture(&postitneutre);
                                }
                                if (j -> input[16-1][0] == 'n') {
                                    postit16.setTexture(&postitnoir);
                                }
                                if (j -> input[16-1][0] == 'b') {
                                    postit16.setTexture(&postitbleu);
                                }
                                if (j -> input[16-1][0] == 'r') {
                                    postit16.setTexture(&postitrouge);
                                }


                                    // 17
                                if (j -> input[17-1][0] == 'w') {
                                    postit17.setTexture(&postitneutre);
                                }
                                if (j -> input[17-1][0] == 'n') {
                                    postit17.setTexture(&postitnoir);
                                }
                                if (j -> input[17-1][0] == 'b') {
                                    postit17.setTexture(&postitbleu);
                                }
                                if (j -> input[17-1][0] == 'r') {
                                    postit17.setTexture(&postitrouge);
                                }


                                    // 18
                                if (j -> input[18-1][0] == 'w') {
                                    postit18.setTexture(&postitneutre);
                                }
                                if (j -> input[18-1][0] == 'n') {
                                    postit18.setTexture(&postitnoir);
                                }
                                if (j -> input[18-1][0] == 'b') {
                                    postit18.setTexture(&postitbleu);
                                }
                                if (j -> input[18-1][0] == 'r') {
                                    postit18.setTexture(&postitrouge);
                                }


                                    // 19
                                if (j -> input[19-1][0] == 'w') {
                                    postit19.setTexture(&postitneutre);
                                }
                                if (j -> input[19-1][0] == 'n') {
                                    postit19.setTexture(&postitnoir);
                                }
                                if (j -> input[19-1][0] == 'b') {
                                    postit19.setTexture(&postitbleu);
                                }
                                if (j -> input[19-1][0] == 'r') {
                                    postit19.setTexture(&postitrouge);
                                }


                                    // 20
                                if (j -> input[20-1][0] == 'w') {
                                    postit20.setTexture(&postitneutre);
                                }
                                if (j -> input[20-1][0] == 'n') {
                                    postit20.setTexture(&postitnoir);
                                }
                                if (j -> input[20-1][0] == 'b') {
                                    postit20.setTexture(&postitbleu);
                                }
                                if (j -> input[20-1][0] == 'r') {
                                    postit20.setTexture(&postitrouge);
                                }


                                    // 21
                                if (j -> input[21-1][0] == 'w') {
                                    postit21.setTexture(&postitneutre);
                                }
                                if (j -> input[21-1][0] == 'n') {
                                    postit21.setTexture(&postitnoir);
                                }
                                if (j -> input[21-1][0] == 'b') {
                                    postit21.setTexture(&postitbleu);
                                }
                                if (j -> input[21-1][0] == 'r') {
                                    postit21.setTexture(&postitrouge);
                                }


                                    // 22
                                if (j -> input[22-1][0] == 'w') {
                                    postit22.setTexture(&postitneutre);
                                }
                                if (j -> input[22-1][0] == 'n') {
                                    postit22.setTexture(&postitnoir);
                                }
                                if (j -> input[22-1][0] == 'b') {
                                    postit22.setTexture(&postitbleu);
                                }
                                if (j -> input[22-1][0] == 'r') {
                                    postit22.setTexture(&postitrouge);
                                }


                                    // 23
                                if (j -> input[23-1][0] == 'w') {
                                    postit23.setTexture(&postitneutre);
                                }
                                if (j -> input[23-1][0] == 'n') {
                                    postit23.setTexture(&postitnoir);
                                }
                                if (j -> input[23-1][0] == 'b') {
                                    postit23.setTexture(&postitbleu);
                                }
                                if (j -> input[23-1][0] == 'r') {
                                    postit23.setTexture(&postitrouge);
                                }


                                    // 24
                                if (j -> input[24-1][0] == 'w') {
                                    postit24.setTexture(&postitneutre);
                                }
                                if (j -> input[24-1][0] == 'n') {
                                    postit24.setTexture(&postitnoir);
                                }
                                if (j -> input[24-1][0] == 'b') {
                                    postit24.setTexture(&postitbleu);
                                }
                                if (j -> input[24-1][0] == 'r') {
                                    postit24.setTexture(&postitrouge);
                                }


                                    // 25
                                if (j -> input[25-1][0] == 'w') {
                                    postit25.setTexture(&postitneutre);
                                }
                                if (j -> input[25-1][0] == 'n') {
                                    postit25.setTexture(&postitnoir);
                                }
                                if (j -> input[25-1][0] == 'b') {
                                    postit25.setTexture(&postitbleu);
                                }
                                if (j -> input[25-1][0] == 'r') {
                                    postit25.setTexture(&postitrouge);
                                }


                                sf::Event event;
                                while (game.pollEvent(event))
                                {
                                    if (event.type == sf::Event::Closed)
                                        game.close();
                                }


                                    // On affiche tous les éléments

                                game.draw(fondsombre);
                                game.draw(postit1);
                                game.draw(postittext1);
                                game.draw(postit2);
                                game.draw(postittext2);
                                game.draw(postit3);
                                game.draw(postittext3);
                                game.draw(postit4);
                                game.draw(postittext4);
                                game.draw(postit5);
                                game.draw(postittext5);
                                game.draw(postit6);
                                game.draw(postittext6);
                                game.draw(postit7);
                                game.draw(postittext7);
                                game.draw(postit8);
                                game.draw(postittext8);
                                game.draw(postit9);
                                game.draw(postittext9);
                                game.draw(postit10);
                                game.draw(postittext10);
                                game.draw(postit11);
                                game.draw(postittext11);
                                game.draw(postit12);
                                game.draw(postittext12);
                                game.draw(postit13);
                                game.draw(postittext13);
                                game.draw(postit14);
                                game.draw(postittext14);
                                game.draw(postit15);
                                game.draw(postittext15);
                                game.draw(postit16);
                                game.draw(postittext16);
                                game.draw(postit17);
                                game.draw(postittext17);
                                game.draw(postit18);
                                game.draw(postittext18);
                                game.draw(postit19);
                                game.draw(postittext19);
                                game.draw(postit20);
                                game.draw(postittext20);
                                game.draw(postit21);
                                game.draw(postittext21);
                                game.draw(postit22);
                                game.draw(postittext22);
                                game.draw(postit23);
                                game.draw(postittext23);
                                game.draw(postit24);
                                game.draw(postittext24);
                                game.draw(postit25);
                                game.draw(postittext25);
                                game.draw(interrupteur2);
                                game.display();

                                if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
                                {
                                        // RECUPERE LA POSITION DE LA SOURIS AU MOMENT DU CLIC
                                    sf::Vector2f mouse = game.mapPixelToCoords(sf::Mouse::getPosition(game));


                                    sf::FloatRect interrupteur = interrupteur2.getGlobalBounds();


                                    if (interrupteur.contains(mouse))
                                    {
                                        game.close();
                                    }

                                }

                            }

                        }

                    }

                }

            }
        }
    }

    return 0;
}
