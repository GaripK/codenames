# codenames

Projet S5 IHM


Nous avons deux versions du même jeu :
 - Une version avec seulement le coeur, qui tourne dans un terminal (version accéssibilité)
 - Une version avec une interface utilisateur


Pour compiler la version d'accéssibilité vous pouvez utiliser le makefile


Pour compiler la version interface, il faut tout d'abord installer SFML :
https://www.sfml-dev.org/tutorials/2.5/start-linux-fr.php

Vous pouvez ensuite utiliser le makefile